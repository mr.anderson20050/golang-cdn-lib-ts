import axios from "axios"

interface CreateBucketResp {
  smt: string
}

interface BucketKeys {
	read?: string[]
	write?: string[]
	delete?: string []
}

interface CreateBucketOpts {
  name: string
	keys: BucketKeys
}

interface operation {
	operation: 'get' | 'post' | 'delete'
	type: 'private' | 'public'
	keys: string[]
}
interface RequestJSON {
	name: string
	module: string
	operations: operation[]
}


function CreateBuckets(url: string, bucket: CreateBucketOpts): RequestJSON {
	// buckets = Array.isArray(buckets) ? buckets : [ buckets ]
	let reqJSON: RequestJSON

	reqJSON = {
		name: bucket.name,
		module: "image",
		operations: [
			{
				operation: 'get',
				type: bucket.keys.read !== undefined ? (bucket.keys.read?.length > 0 ? "public" : "public") : "private",
				keys: bucket.keys.read || []
			}, {
				operation: 'post',
				type: bucket.keys.write !== undefined ? (bucket.keys.write?.length > 0 ? "public" : "public") : "private",
				keys: bucket.keys.write || []
			}, {
				operation: 'delete',
				type: bucket.keys.delete !== undefined ? (bucket.keys.delete?.length > 0 ? "public" : "public") : "private",
				keys: bucket.keys.write || []
			}
		]
	}

  axios.post(`${url}/api/bucket`, JSON.stringify(reqJSON), {
  	headers: {
    	'Content-Type': 'application/json'
  	}
	})
  return reqJSON
}

export { CreateBuckets }
export type { CreateBucketResp, CreateBucketOpts, BucketKeys }
