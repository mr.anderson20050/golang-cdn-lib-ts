/*
████████╗ ██████╗ ██████╗  ██████╗ 
╚══██╔══╝██╔═══██╗██╔══██╗██╔═══██╗
   ██║   ██║   ██║██║  ██║██║   ██║
   ██║   ██║   ██║██║  ██║██║   ██║
   ██║   ╚██████╔╝██████╔╝╚██████╔╝
   ╚═╝    ╚═════╝ ╚═════╝  ╚═════╝
*/


interface BucketKeys {
  upload?: string 
  delete?: string
  read?: string
}
interface Bucket {
  name: string
  keys?: BucketKeys
}

// NOTICE: May be used when different CDNs will have different buckets
// interface CdnParams {
//   url: string
//   buckets: Bucket[]
// }

interface ClientOpts {
  // NOTICE: May be used when different CDNs will have different buckets
  // cdnParams: CdnParams[]
  buckets: Bucket[]
  masterCDN: string
}



class BucketManager {
  url: string; 
  buckets: Bucket[] = [];


  getBucket(bucketName: string): Bucket | undefined {
    return this.buckets.find(key => key.name === bucketName);
  }


  addExistingBuckets(buckets: Bucket[]) {
    buckets = Array.isArray(buckets) ? buckets : [ buckets ]

    for(let bucket of buckets) {
      if (!this.getBucket(bucket.name)) { 
        return console.log(`[Warn] addExistingBuckets: Bucket ${bucket.name} already exists!`) 
      }
      this.buckets = [...buckets, bucket]
    }
    process.env.BUCKETS = JSON.stringify(buckets)
  }

  constructor(options: ClientOpts) {
    this.url = options.masterCDN
    if(process.env.BUCKETS != undefined) {
      this.buckets = JSON.parse(process.env.BUCKETS)
    }
    this.addExistingBuckets(options.buckets || [])
  }
}



export { BucketManager }
export type { BucketKeys, Bucket, ClientOpts}