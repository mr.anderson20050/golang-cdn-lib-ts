import axios from "axios"; 
import fs from "fs"
import { Stream } from "stream";

async function GetFileStream(id: string, url: string, bucket: string): Promise<Stream> {
  try {
    let resp = await axios({
      method: "GET",
      url: `${url}/${bucket}/${id}`,
      responseType: "stream"
    })
    return resp.data // data should be a stream
  } catch(err) {
    throw err
  }
}

async function DownloadFile(pathToSave: string, id: string, url: string, bucket: string): Promise<void> {
  let data = await GetFileStream(id, url, bucket)
  data.pipe(fs.createWriteStream(pathToSave))
}

export { DownloadFile, GetFileStream }
