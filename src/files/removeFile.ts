import axios from "axios";

async function RemoveFile(fileId: string, url: string, bucket: string, key?: string) {
  try {
    await axios.delete(`${url}/${bucket}/${fileId}`)
    return
  } catch(err) {
    throw err
  }
}

export {RemoveFile}