import axios, { AxiosRequestConfig } from "axios";
import FormData from 'form-data';
import { Stream } from "stream";
import { StreamBuffering, RandStr } from "../tools"
import fs from 'fs'


interface SendFilesResp {
  ids: string[],
  urls: string[]
}


const SendFilesFromFormData = async ( 
  data: FormData, 
  url: string,
  bucket: string,
  key?: string,
): Promise<SendFilesResp> => {
  const opts: AxiosRequestConfig = {
    method: "POST",
    url: `${url}/${bucket}/`,
    data,
    headers: {
      ...data.getHeaders()
    }
  }

  try {
    let res = await axios(opts)
    return res.data
  } catch (err) {
    throw err
  }
}




const SendFilesFromStorage = async (
  paths: string[] | string, 
  url: string, 
  bucket: string,
  isAsync: boolean = false,
  key?: string,
): Promise<SendFilesResp> => {
  const formData = new FormData();
    
  // Change paths into array if it's not
  const pathsArr: string[] = Array.isArray(paths) ? paths : [ paths ];
  // FOR ASYNC FILE UPLOAD
  if(isAsync) { 
    let filesReaded: number = 0;

    return new Promise<SendFilesResp>((res, rej) => {
      pathsArr.map(async (path, id) => {
        const fileName = path.replace(/^.*(\\|\/|\:)/, '');
        fs.readFile(path, async (err, bf) => {
          if(err) rej(err);

          const isLast = ++filesReaded === pathsArr.length
          
          formData.append('file', bf, fileName);
          if(isLast) res(await SendFilesFromFormData(formData, url, bucket, key))
        })
      })
    })
  }
  // FOR SYNC FILE UPLOAD
  pathsArr.map((path) => {
    try {
      const fileName = path.replace(/^.*(\\|\/|\:)/, '');
      formData.append('file', fs.readFileSync(path), fileName);
    } catch(err) { throw `Failed to add file ${path} into FormData! Err: ${err}` };
  });
  return await SendFilesFromFormData(formData, url, bucket, key); 
}




const SendFilesFromStreams = async (
  streams: Stream[] | Stream, 
  url: string, 
  bucket: string,
  key?: string,
): Promise<SendFilesResp> => {
  const formData = new FormData();
  
  // Change streams into array if it's not
  streams = Array.isArray(streams) ? streams : [ streams ]

  streams.map(async (stream) => {
    try {
      const fileBuffers = StreamBuffering(stream)
      formData.append("file", fileBuffers, RandStr(12))
    } catch (err) { throw err }
  })
  return await SendFilesFromFormData(formData, url, bucket, key)
}



export { SendFilesFromStorage, SendFilesFromStreams, SendFilesFromFormData }
export type { SendFilesResp }
