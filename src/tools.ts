import { Stream } from "stream";


function StreamBuffering(stream: Stream): Promise<Buffer> {
  var chunks: Buffer[] = [];

  return new Promise((resolve, reject) => {
    stream.on('data', (chunk: Buffer) => { 
      chunks = [...chunks, Buffer.from(chunk)];
    });
    stream.on('end', () => resolve( Buffer.concat(chunks) ));
    stream.on('error', (err) => reject(err));
  });
};


function RandStr(size: number): string {
  let res: string = "";
  let characters: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for ( let i: number = 0; i < size; i++ ) {
      res += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return res;
}


export { StreamBuffering, RandStr }