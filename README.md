# Animakuro CDN

## Install
``` npm i @4nd3rs0n/animakuro-cdn ```

## About animakuro-cdn
It's just like S3 storage but with some extra-features  
This library is result of work in startup named "Animakuro" wich is online streaming anime service. We made it opensource because owner of this startup decieved all the developers. He promised money which he didn't have, he spoke about investors that never existed. CEO of this project Igor or Michael or Pavel (he introduced himself every time differently) is pidoras. Never work with Animakuro it's scammers!  
Repository of the CDN: https://github.com/sonyamoonglade/cdn-go  
Also we are developing same library for golang: https://gitlab.com/mr.anderson20050/golang-cdn-lib

## Upload file into CDN
### File from disk
```ts
import { SendFiles } from "animakuro-cdn";
const url = "https://cdn.example.com"; // DON'T ADD "/" AT THE END!

const someAsyncFunc = async () => {
  // SendFilesFromStorage(<paths to files>, <CDN url>, <bucket>, <isAsync (default: false)>)
  let responce = await SendFilesFromStorage("./file.txt", url, "site-content", true)

/*
  - CDN url is a address of CDN.
  - "site-content" in this code is a default bucket of animakuro CDN. Buckets are the place where CDN store files. Some of the buckets can be private (can write something there only providing key). So changing `site-content` will change location of your files on the CDN. You can set it for exaple to "images".
  - isAsync can be used for async file loading. Default value is false. It's useful to set it true if you have large or many files.
*/

  // You can also use array as a parameter
  let responce = await SendFilesFromStorage(["./file.txt", "file2.txt", "file3.txt"], url, "site-content")
}
```


### File from stream
``` ts
import { SendFiles } from "animakuro-cdn";

const someAsyncFunc = async () => {
  // SendFilesFromStream(<streams>, <CDN url>, <bucket>)
  let responce = await SendFilesFromStream(fs.createReadStream(filename), url, "site-content")

  // You can also use array as a parameter
  let responce = await SendFilesFromStorage([someReadStrem(), someReadStream()], url, "site-content")
}
```

### File from formData
``` ts
import FormData from 'form-data';

const someAsyncFunc = async () => {

  const responce = await SendFilesFromForm(stream, url, "site-content")
}
```

## Get file from CDN
### To a file:
``` ts
import { DownloadFile, GetFileStream, DownloadFile } from "animakuro-cdn";

DownloadFile("../path/to/save.txt", id, url, bucket)
let stream = await GetFileStream(id, url, bucket)
```

## Remove file from cdn
``` ts
import { RemoveFile } from "animakuro-cdn";

RemoveFile(id, url, bucket)
```
